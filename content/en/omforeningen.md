# About the association
Open Source means a revolution for the global IT industry. Software development is streamlined, cost effectiveness increases and systems are developed better in close collaboration between suppliers, customers and end users.

The Open Source movement is also growing in Sweden. The Association of Suppliers of Open Software in Sweden, Open Source Sweden, has been formed to protect the interests of Swedish suppliers.
What?
We want to contribute to a favorable development for suppliers and customers so that they will have increased cost efficiency and competitive advantages in their operations.
Our purpose is to promote a well-functioning national and international market for the development, sale, delivery and support of open software and services. A market comprising both private and public organisations.

## For who?
Open software affects more and more segments of the IT industry. Many companies have operations that in one way or another use open source software in their business models. All of these may have an interest in involvement in the association. Examples where open software is already an essential part of the offer today are software companies, system integrators, consulting companies, hardware and infrastructure companies, operating companies, educational companies.

## How?
The work within the association is conducted in accordance with the annual business plan established by the board. The practical work is carried out partly through member meetings which are held every quarter, partly through various working groups which, on a voluntary basis, run various issues and activities.

An important part of the association's activities are conferences.

## Member?
All companies that have professional activities can apply for membership. Here on the web you can also find our statutes and information about membership fees etc.

Welcome with your application!

## Annual minutes
* 2018: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2018.pdf
* 2017: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2017.pdf
* 2016: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2016.pdf
* 2015: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2015.pdf
* 2014: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2014.pdf
* 2013: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2013.pdf
* 2012: https://github.com/opensourcesweden/opensourcesweden.github.io/blob/master/docs/oss-protocol-2012.pdf

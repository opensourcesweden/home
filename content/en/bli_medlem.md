# Sign up
Send your application according to the template below via e-mail to styrelsen@opensourcesweden.org. Your application is then processed by the board. All companies that have professional activities can apply for membership.

Through this application, you approve the association's statutes [(in link here)](https://opensourcesweden.github.io/stadgar/) and that you support the wording in Open Source Sweden's "Mission statement [(in link here)]( https://opensourcesweden.github.io/stadgar/)

## Current fee
(Determined at the annual meeting)
```
Membership fee: SEK 500 / company
`# Sign up
Send your application according to the template below via e-mail to styrelsen@opensourcesweden.org. Your application is then processed by the board. All companies that have professional activities can apply for membership.

Through this application, you approve the association's statutes [(in link here)](https://opensourcesweden.github.io/stadgar/) and that you support the wording in Open Source Sweden's "Mission statement [(in link here)]( https://opensourcesweden.github.io/stadgar/)

## Current fee
(Determined at the annual meeting)
```
Membership fee: SEK 500 / company
```
```
Service fee is calculated on the number of employees who make up Open Source capacity.
(VAT is added)
 
< 6 employees SEK 7,500 9 employees SEK 11,500
 6 employees SEK 8,500 10 employees SEK 12,500
 7 employees SEK 9,500 11-25 employees SEK 17,500
 8 employees SEK 10,500 >25 employees SEK 27,500
```

## Application to become a member
Fill in the information below in your application and email elina@sinf.se, subject line: "Membership application: Open Source Sweden".

```
The undersigned company hereby applies for membership in the Association of Providers of Open Software in Sweden,
Open Source Sweden. Through this application, we approve the association's statutes and that we stand behind them
the wording in Open Source Sweden's "Mission statement".

Company name
Org. no.
Address
ZIP code
Contact person
Email to contact person
Phone
Website

Number of full-time employees in Sweden who work with Open Source-related activities.
("Open Source Capability", which is chargeable)

Attach a logo with a fixed width of 150 pixels. The logo must be in jpg, gif or png format.
```


## Withdrawal rules
A member who wishes to withdraw from the association must report this in writing to the association's board no later than 3 months before the turn of the year at which the membership is intended to end. Reimbursement of paid annual fee does not take place.``
```
Service fee is calculated on the number of employees who make up Open Source capacity.
(VAT is added)
 
< 6 employees SEK 7,500 9 employees SEK 11,500
 6 employees SEK 8,500 10 employees SEK 12,500
 7 employees SEK 9,500 11-25 employees SEK 17,500
 8 employees SEK 10,500 >25 employees SEK 27,500
```

## Application to become a member
Fill in the following information in your application

```
The undersigned company hereby applies for membership in the Association of Providers of Open Software in Sweden,
Open Source Sweden. Through this application, we approve the association's statutes and that we stand behind them
the wording in Open Source Sweden's "Mission statement".

Company name
Org. no.
Address
ZIP code
Contact person
Email to contact person
Phone
Website

Number of full-time employees in Sweden who work with Open Source-related activities.
("Open Source Capability", which is chargeable)

Attach a logo with a fixed width of 150 pixels. The logo must be in jpg, gif or png format.
```


## Withdrawal rules
A member who wishes to withdraw from the association must report this in writing to the association's board no later than 3 months before the turn of the year at which the membership is intended to end. Reimbursement of paid annual fee does not take place.

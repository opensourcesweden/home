---
title: Our new English speaking website
date: 2023-11-13
subtitle: The tech behind opensourcesweden.org
tags: [blogg]
---

### The tech behind our new website

We have spent the last few days moving our older webpage to a new platform. Our webpage and hosting platform is open source inspired. The site itself is hosted on GitLab, built using GitLab's Linux powered CI/CD and the Static Page Generator, Hugo.

Hugo suits our needs of something which is simple, renders quickly and which has a focus on privacy. Hugo generates static HTML pages, which is what you see, when you visit this website. Static HTML pages, with no tracking, no cookies and strong HTTPS encryption (which is forced for all our users) ensures the page renders quickly and protects our users privacy. 

Hugo is ofcourse an open source project, which is available here: https://gitlab.com/pages/hugo. The page source code is kept in this project on GitLab https://gitlab.com/opensourcesweden/home for full transparency.

Hope you enjoy it.

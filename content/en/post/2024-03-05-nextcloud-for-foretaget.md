---
title: Nextcloud for your company - How does it work?
date: 2024-04-05
subtitle: This session features Dwellirs experience with selecting a solution based on the open source based solution NextCloud
tags: [kalenderium, blogg]
---

### Dwellir and NextCloud for your company - how does it work?

Dwellir AB is a company in the crypto industry founded in 2021 with approx. 15 people, employees and consultants. Dwellir uses Nextcloud as an overall solution for the company's office IT, i.e. e-mail, calendar, video conferencing, planning, file sharing and document management. In this webinar with Open Source Sweden, Erik Lönroth (CTO) explains what was the basis for the decision to choose Nextcloud, what the employees think and how it works today.

A link to connect to the ectn can be found on our [LinkedIn page](https://www.linkedin.com/posts/open-source-sweden_inneh%C3%A5ll-dwellir-ab-%C3%A4r-ett-f%C3%B6retag-inom-activity-7170700942856634369-p3AX/)

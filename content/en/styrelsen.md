# The board
After the 2023 general meeting, the association's board consists of:

## Chairman
```
Björn Lundell, Researcher at the University of Skövde, bjorn.lundell@his.se
```

## Vice Chairman
```
Tony Nicolaides, Redpill Linpro, tony.nicolaides@redpill-linpro.se
```

## Board members
```
Tomas Gustavsson, Keyfactor, tomas@keyfactor.se
Jonas Feist, Redbridge, jfeist@redbridge.se
Magnus Glantz, Red Hat AB, sudo@redhat.com
Mathias Lindroth, ACF Legal Intl AB, m@acf.legal
Erik Lönroth, Dwellir AB, erik.lonroth@Dwellir.se
Jonas Larsson, Frontwalker AB, jonas.larsson@frontwalker.se
```

## Treasurer
```
Jonas Feist, Redbridge, jfeist@redbridge.se
```

## Substitutes
```
Colin Campbell, Digitalist AB, colin.campbell@digitalist.se 
Johan Bernhardsson, Kafit AB, johan@kafit.se 
```

## Nomination committee
```
Jonas Larsson, Frontwalker AB, jonas.larsson@frontwalker.se
Jonas Feist, deputy CEO, Redbrigde AB, onas.feist@redbridge.se
```

## Auditors
```
Malena Wegin, Revidens Konsult AB
```

# Medlemsföretag
Här finner du alla föreningens företag.

## [Digitalist](https://www.digitalist.se)
Vi är en digital partner med fokus på affärsnytta och kundupplevelse. Med innovation som drivkraft och spetskompetens inom insikter, teknik och design framtidssäkrar vi er verksamhet. Vi utvecklar och förvaltar appar, webbplatser och digitala tjänster.

## [Dwellir AB](https://www.dwellir.com)
A pioneering company in the realm of blockchain infrastructure. Founded in Uppsala, Sweden in 2021 by Gustav Nipe, Joakim Nyman and Erik Lönroth, our focus is on the future of the internet, specifically the development of infrastructure for blockchain networks like Polkadot and Ethereum.

## [imCode Partner AB](https://imcode.com)
Vi fokuserar på att underlätta och möjliggöra digital delaktighet, i första hand inom offentlig sektor. Att vara delaktig, att arbeta tillsammans, skapar ömsesidig förståelse, ger bättre resultat och ett verkligt inflytande över individens situation.

## [Kafit AB](https://kafit.se)
Kafit är litet av en doldis i branschen där vi som egenskap av underleverantör genom åren har levererat kompetens, system och hårdvara till en mängd olika stora välkända företag inom tillverkningsindustrin och tjänstesektorn.

## [Monator Technologies AB](http://www.monator.com)
We are change makers and act as catalysts helping organisations reshape how they work, learn and collaborate to better serve and engage with their customers and employees.

## [ACF Legal](https://www.linkedin.com/in/mathiaslindroth/details/experience/?locale=sv_SE/)

## [PrimeKey Solutions AB](https://www.primekey.com/)
One of the world’s leading companies for PKI solutions, PrimeKey has developed successful technologies, such as EJBCA® Enterprise, SignServer Enterprise and PrimeKey® EJBCA Appliance.

## [Red Hat AB](https://www.redhat.com)
Vi är världens ledande leverantör av öppenkällkodsbaserade företagslösningar. Med världens öppenkällkodsekosystem så levererar vi högpresterande Linux, cloud, container och Kuberneteslösningar.

## [RedBridge AB](https://www.redbridge.se)
RedBridge vision är att förändra IT marknaden. Vår utgångspunkt är att öppna standarder och Open Source ger våra kunder större valfrihet idag och i framtiden.

## [Redpill Linpro AB](https://www.redpill-linpro.com)
We help our customers to build the IT foundation for their digital transformation in the areas API & Integration, DevOps, and Cloud. As the leading Open Source service provider in the Nordics, Redpill Linpro always has a focus on openness.

## [Scania](https://www.scania.com/)
Scania is a world leading provider of transport solutions with 50,000 employees in about 100 countries. Together with our partners and customers we are driving the shift towards a sustainable transport system.

## [South Pole AB](https://southpole.se/)
Based in Stockholm, South Pole has over fifteen years’ experience of delivering high performance and enterprise computing solutions to businesses across the Nordics.

## [Keitaro](https://keitaro.com/)
Keitaro is an open source oriented company that develops solutions empowering governments, organizations and enterprises around the world and across multiple industries. Founded in 2012 in Sweden, Keitaro now operates in different countries across the world. We offer open-source expertise gained from years of experience and as a company that is fully dedicated to open-source software, our competence is integrated in every project we undertake.

At Keitaro, we believe every idea can be turned into a solution. We take the time to understand and acknowledge our client’s goals and then work out the best way to achieve them, using a collaborative approach. We push the limits to create scalable solutions of the highest standard, whilst closely following each client’s vision. Aside from our work on commercial projects, it’s important to us that we give back to the open-source community. lo
For us, success involves constant learning, teamwork, growth and ensuring we stay devoted to the values of openness.


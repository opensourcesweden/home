![Open Source Sweden](/osslogo.png)

## Hejsan
Open Source Sweden är en branchorganisation som stöttar intressna av svenska leverantörer av öppen källkod och öppen programvara.

Här hittar du allt du vill veta om vår organisation, inklusive vad vi gör.

---
title: Redpill Linpro grundar European Open Source Consortium
date: 2024-06-14
subtitle: Redpill Linpro har tillsammans med några europeiska Open Source bolag grundat European Open Source Consortium (EOS Consortium)
tags: [blogg]
---

### European Open Source Consortium

Redpill Linpro är medgrundare till "European Open Source Consortium" tillsammans med våra betrodda partners Smile (Frankrike), Adfinis (Schweiz), Univention (Tyskland) och Lunatech Consulting Ltd (NL, Storbritannien och FR).

Tillsammans har organisationerna verksamhet i över 15 europeiska länder och mer än 3 000 digitaliseringsspecialister i regionen, med fokus på konsulting, utveckling, optimering och drift av storskaliga open source-projekt. Genom ett ramavtal med en av konsortiemedlemmarna kan kunder få tillgång till det samlade kompetensutbudet som tillhandahålls av alla konsortiemedlemmar.

Konsortiet arbetar gemensamt för att tillgodose behovet av en pan-europeisk digitaliseringspartner och tjänsteleverantör på enterprise-nivå som specialiserar sig på öppen källkod och öppna standarder, med fokus på företag och offentliga förvaltningar.
"Med European Open Source Consortium tar vi bort en av de vanligaste invändningarna mot att anskaffa och införa storskaliga implementeringsprojekt baserade på open source – att det inte finns några europeiska open source-partners på enterprise-nivå", säger Fredrik Svensson, affärsutvecklingschef på Redpill Linpro.

För mer information och för att se hur EOS Consortium kan hjälpa din organisation, besök vår webbplats: https://eos-consortium.com och följ oss på LinkedIn: https://lnkd.in/dXAmmJ-j

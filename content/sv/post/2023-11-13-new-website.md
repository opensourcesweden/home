---
title: Vår nya webbsida
date: 2023-11-13
subtitle: Tekniken bakom opensourcesweden.org
tags: [blogg]
---

### Tekniken bakom vår webbsida

Vi har spenderat de senaste par dagarna med att flytta vår gamla webbsida till en ny plattform. Vår webbsida och hostingplattformen är öppen-källkodsinspirerad. Sidan själv är hostad på GitLab, byggd med GitLabs Linux-drivna CI/CD och Hugo, en Static Page Generator.

Hugo passar våra behov, vilket är något som är enkelt, renderar snabbt och har ett focus på privacy. Hugo generar statiska HTML-sidor, vilket är vad du ser, när du besöker sidan. Statiska HTML-sidor, ingen tracking, inga kakor och stark kryptering (vilket tvingas för alla besökare) är en garant för att sidan både är snabb och säker.

Hugo är såklart ett öppen källkodsprojekt, vilket är tillgängligt här: https://gitlab.com/pages/hugo. Vår sidas källkod lagras även på GitLab https://gitlab.com/opensourcesweden/home för full transparens.

Hoppas att du gillar det.

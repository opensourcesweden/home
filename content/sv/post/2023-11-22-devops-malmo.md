---
title: Open Source Sweden presenterar på DevOps Malmö
date: 2023-11-22
subtitle: Träffa oss på Foo Café 14:e december och prata om öppenkällkod
tags: [kalenderium, blogg]
---

### Open Source Sweden presenterar på DevOps Malmö

Open Source Swedens styrelseledamöter Mathias Lindroth och Magnus Glantz kommer att presentera på DevOps Malmös nästa meetup på Foo Café i Malmö, 14:e december, 17.30.

Talarna kommer att leda dig genom alla viktiga saker du behöver veta när du startar eller kör ett projekt med öppen källkod. Vi kommer att diskutera grundläggande överväganden för framgångsrika projekt med öppen källkod, som:
* Projektmål och marknadspositionering
* Projektidentitet och varumärke
* Styrning
* Infrastruktur och finansiering
* Mätvärden och hållbarhet
* Licensiering och allt lagligt

Det här är en användbar session för alla:
* som vill starta ett projekt med öppen källkod
* som för närvarande driver ett projekt och letar efter sätt att förbättra det
* som vill förstå bättre hur man kan bidra till projekt med öppen källkod
* som är nyfikna på grunderna som gör projekt med öppen källkod

Registrera dig här: https://www.meetup.com/devopsmalmo/events/297499298/

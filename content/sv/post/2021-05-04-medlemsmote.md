---
title: Medlemsmöte om öppen källkod och anställningsavtal
date: 2021-05-04
subtitle: Anställningsavtal diskriminerar mot öppen källkod
tags: [aktivitet, anställningsavtal, 2021]
---

## Medlemsmöte om öppen källkod och anställningsavtal

Massa information om medlemsmötet... fyll på.

[Klicka här för att komma till Magnus presentation](https://github.com/mglantz/employment-contacts-oss/raw/main/FLOSS%20diskriminering.pdf).

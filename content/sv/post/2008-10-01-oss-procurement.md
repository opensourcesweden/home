---
title: OSS Procurement guidelines
date: 2008-10-01
subtitle: OSS Procurement guidelines
tags: [artiklar]
---

### Dokument: OSS Procurement guidelines

[https://github.com/opensourcesweden/opensourcesweden.github.io/raw/master/docs/OSS-procurement-guideline-public-draft-v1%201.pdf](https://github.com/opensourcesweden/opensourcesweden.github.io/raw/master/docs/OSS-procurement-guideline-public-draft-v1%201.pdf)

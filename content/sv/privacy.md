# Privacy och GDPR
Sidan är designad med privacy i tanke och följer best practices från PdB (Privacy by Default eller Privacy by Design).

```
Vår webbsida samlar inte in personligt data från besökare, överhuvudtaget.
```

Sidan använder Hugo, som är en "static page generator". Vår webbsida använder därmed inte en databas utan består bara av statiska sidor.
Sidan hämtar och behandlar inget privat data från sidans besökare och är konfigurerad för att inte använda cookies, trackers eller tredjepartsintegrationer.

För frågor om privacy och vår webbsida, kontakta styrelsen@opensourcesweden.se.

# Kryptering
All trafik till hemsidan görs över starkt krypterad HTTPS. Vi tvingar all trafik att krypteras.

Vårt certifikat för opensourcesweden.org kommer från https://letsencrypt.org/ och förnyelsen av certifikatet hanteras automatiskt av GitLab. Vi använder 2048 bitars Elliptic Curve för hemsidans certifikat och den privata nyckeln skyddas av 4096 bitars RSA. 

# Ta inte vårt ord för det
Du kan själva se hur vi har avaktiverat all spårande teknologi på hemsidan.

* Granska hur vi konfigurerat hemsidan: https://gitlab.com/opensourcesweden/home/-/blob/master/config.toml
* Granska all programkod för hemsidan: https://gitlab.com/opensourcesweden/home/
* Granska vilka förändringar som byggts in i hemsidan: https://gitlab.com/opensourcesweden/home/-/pipelines
